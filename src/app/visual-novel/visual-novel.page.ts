import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../providers/chat-service';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import { Events } from '../../providers/events';

@Component({
  selector: 'app-visual-novel',
  templateUrl: './visual-novel.page.html',
  styleUrls: ['./visual-novel.page.scss'],
})
export class VisualNovelPage implements OnInit {
  novelSix: any;
  novelSeven: any;
  novelObj: any;
  novelSixtext: string;
  novelSeventext: string;
  sixCounter: number;
  sevenCounter: number;
  audioParam = 'sound1.ogg';
  visualNovelChapternum: any;
  visualNovelLength: any;
  alertOk = false;
  storypointerDay = 0;
  subscription: any;

  constructor(public chatService: ChatService, public events: Events, private storage: Storage,
    private alertCtrl: AlertController, public router: Router, private route: ActivatedRoute, private platform: Platform, private screenOrientation: ScreenOrientation) {
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.visualNovelChapternum = this.router.getCurrentNavigation().extras?.state.user.day;
      }
    });
  }

  ngOnInit() {
    this.getJsonVisualNovel();
  }

  ionViewDidEnter() {
    if (this.visualNovelChapternum === 7 && this.screenOrientation.type === 'portrait-primary') {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    }

    this.chatAudio();
    this.getJsonVisualNovel();
    this.storage.get('sixCounter').then((data) => {
      if (data !== null && !isNaN(data)) {
        if (this.visualNovelChapternum === 6) {
          this.sixCounter = data;
          this.novelSix = this.novelObj?.array[data - 1].novelId;
          this.novelSixtext = this.novelObj?.array[data - 1].message;
        }
      }
    });

    this.storage.get('sevenCounter').then((data) => {
      if (data !== null && !isNaN(data)) {
        if (this.visualNovelChapternum === 7) {
          this.sevenCounter = data;
          this.novelSeven = this.novelObj.array[data - 1].novelId;
          this.novelSeventext = this.novelObj.array[data - 1].message;
        }
      }
    });

    this.storage.get('tempAudioStore').then((lastAudio) => {
      if (lastAudio) {
        if (lastAudio[0].chapterNum === this.visualNovelChapternum) {
          this.audioParam = lastAudio[0].lastAudioParam;
        }
        else {
          this.audioParam = 'sound1.ogg'
        }
      }
      else {
        this.audioParam = 'sound1.ogg'
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.events.publish('backPressed');
      const chatAudio = this.getChatAudio();
      chatAudio.muted = true;

      this.storage.get('storypointerDayDB').then((val) => {
        if (!isNaN(val) && val === this.visualNovelChapternum) {
          const lastAudio = [{ chapterNum: this.visualNovelChapternum, lastAudioParam: this.audioParam }];
          this.storage.set('tempAudioStore', lastAudio);
        }
      });
      this.router.navigate(['tabs/tabs/home']);
    });
  }

  ionViewWillLeave() {
    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;
    this.subscription.unsubscribe();

    if (this.visualNovelChapternum === 7 && this.screenOrientation.type === 'landscape') {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
  }

  chatAudio = () => {
    const chatAudio = this.getChatAudio();
    chatAudio.muted = false;
    chatAudio.play();
  };

  getChatAudio = () => {
    return document.getElementById('chatAudio') as HTMLAudioElement;
  };

  getJsonVisualNovel = () => {
    this.chatService.getVisualNovel(this.visualNovelChapternum)
      .subscribe((data) => {
        this.novelObj = data;
        this.visualNovelLength = data['array'].length;
      });
  };

  homeIconClick = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.events.publish('backPressed');

    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val) && val === this.visualNovelChapternum) {
        const lastAudio = [{ chapterNum: this.visualNovelChapternum, lastAudioParam: this.audioParam }];
        this.storage.set('tempAudioStore', lastAudio);
      }
    });

    this.router.navigate(['tabs/tabs/home']);
  };

  setUIBackButtonAction() {
    // this.backButton.onClick = () => {
    //   this.events.publish('backPressed');
    //   //  this.chatService.publishChatBackPressData('backPressed');
    // };

    if (this.alertOk) {
      const chatAudio = this.getChatAudio();
      chatAudio.muted = true;
      // this.events.publish('backPressed');
      //  this.chatService.publishChatBackPressData('backPressed');
      this.router.navigate(['tabs/tabs/home']);
    }
  }

  async notifyPlayer(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'cancel'
        },
        {
          text: 'OK',
          handler: () => {
            this.alertOk = true;
            this.setUIBackButtonAction();
          }
        },
      ]
    });

    await prompt.present();
  }

  visualNovelClick = () => {
    if (this.visualNovelChapternum === 6) {
      if (this.sixCounter < this.visualNovelLength) {
        if (this.novelObj?.array[this.sixCounter]?.interimAudioInStory === 'yes') {
          this.audioParam = this.novelObj.array[this.sixCounter].audioSrc;
        }

        this.novelSix = this.novelObj?.array[this.sixCounter]?.novelId;
        this.novelSixtext = this.novelObj?.array[this.sixCounter]?.message;
        this.sixCounter++;
        this.storage.set('sixCounter', this.sixCounter);
      }
      else {
        this.storage.get('storypointerDayDB').then((val) => {
          if (val === this.visualNovelChapternum) {
            this.notifyPlayer(`You have reached the end of story ${this.visualNovelChapternum}. Go to next Chapter`);
            this.storypointerDay = val + 1;
            this.storage.set('storypointerDayDB', this.storypointerDay);
            this.storage.set('sendthssDB', 0);
          }
          else {
            this.notifyPlayer(`You have reached the end of story ${this.visualNovelChapternum}. Go to next Chapter`);
          }
        });
      }
    }
    else {
      if (this.sevenCounter < this.visualNovelLength) {
        if (this.novelObj?.array[this.sevenCounter]?.interimAudioInStory === 'yes') {
          this.audioParam = this.novelObj.array[this.sevenCounter].audioSrc;
        }

        this.novelSeven = this.novelObj?.array[this.sevenCounter]?.novelId;
        this.novelSeventext = this.novelObj?.array[this.sevenCounter]?.message;
        this.sevenCounter++;
        this.storage.set('sevenCounter', this.sevenCounter);
      }
      else {
        this.storage.get('storypointerDayDB').then((val) => {
          if (val === this.visualNovelChapternum) {
            this.notifyPlayer(`You have reached the end of story ${this.visualNovelChapternum}. Go to next Chapter`);
            this.storypointerDay = val + 1;
            this.storage.set('storypointerDayDB', this.storypointerDay);
            this.storage.set('sendthssDB', 0);
          }
          else {
            this.notifyPlayer(`You have reached the end of story ${this.visualNovelChapternum}. Go to next Chapter`);
          }
        });
      }

    }
  };
}

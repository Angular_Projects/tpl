import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualNovelPage } from './visual-novel.page';

const routes: Routes = [
  {
    path: '',
    component: VisualNovelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualNovelPageRoutingModule {}

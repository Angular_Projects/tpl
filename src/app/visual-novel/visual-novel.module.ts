import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisualNovelPageRoutingModule } from './visual-novel-routing.module';

import { VisualNovelPage } from './visual-novel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualNovelPageRoutingModule
  ],
  declarations: [VisualNovelPage]
})
export class VisualNovelPageModule {}

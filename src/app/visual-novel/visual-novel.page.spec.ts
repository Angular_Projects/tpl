import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisualNovelPage } from './visual-novel.page';

describe('VisualNovelPage', () => {
  let component: VisualNovelPage;
  let fixture: ComponentFixture<VisualNovelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualNovelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisualNovelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

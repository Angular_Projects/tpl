import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-journals',
  templateUrl: './journals.page.html',
  styleUrls: ['./journals.page.scss'],
})
export class JournalsPage implements OnInit {
  feedData = [];
  subscription: any;

  constructor(private storage: Storage, private platform: Platform, public router: Router, private screenOrientation: ScreenOrientation) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidEnter() {
    this.storage.get('feedData').then((data) => {
      const array = new Array(data);

      if (array[0]) {
        array.forEach((items) => {
          this.feedData = [];
          for (const i of items) {
            this.feedData.push(i);
          }
        });
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/tabs/tabs/journal']);
    });
  }

  journalBackClick = () => {
    this.router.navigate(['/tabs/tabs/journal']);
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }
}

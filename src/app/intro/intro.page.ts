import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ModalController, IonSlides } from '@ionic/angular';
import { ChatService } from '../../providers/chat-service';
import { HabitModalPage } from '../habit-modal/habit-modal.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 10,
    centeredSlides: true
  };
  defaultBehaviours: any;
  challenges: any;
  slideContents: any;
  isSelected: boolean;

  @ViewChild('slides', { static: false }) slides: IonSlides;

  constructor(public chatService: ChatService, private storage: Storage, private screenOrientation: ScreenOrientation, public router: Router, public modalController: ModalController) {
    this.defaultBehaviours = {
      happiness: 0.3,
      health: 0.6,
      romance: 0.2,
      fear: 0.3,
      wish: 0,
      wantedLevel: 0,
      balance: '$5744.00'
    };

    this.storage.set('defaultBehaviour', this.defaultBehaviours);
    this.storage.set('storypointerDayDB', 1);  // Replay story
    this.storage.set('sendthssDB', 0);
    this.storage.set('findFateDB', 'XXXXXXXX');
    this.storage.set('todayDateDB', 1);
    this.storage.remove('fill');  // Replay story
    this.storage.set('narrateID', 0);
    this.storage.set('disclaimerCounter', 0);
    this.storage.set('graphicCount', 1);
    this.storage.set('graphicOverflowCount', 1);
    this.storage.set('sixCounter', 1);
    this.storage.set('sevenCounter', 1);
    // this.storage.set('darkToggleValue', true);

  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.challenges = [
      {
        id: 1,
        challenge: '7 minutes workout',
        challengeCheck: false
      },
      {
        id: 2,
        challenge: 'Learn a new language',
        challengeCheck: false
      },
      {
        id: 3,
        challenge: 'Digital declutter',
        challengeCheck: false
      },
      {
        id: 4,
        challenge: 'Webcomics with Inkscape',
        challengeCheck: false
      },
      {
        id: 5,
        challenge: 'Journaling',
        challengeCheck: false
      },
      {
        id: 6,
        challenge: 'Read a book',
        challengeCheck: false
      },
      {
        id: 7,
        challenge: 'Write a book',
        challengeCheck: false
      },
      {
        id: 8,
        challenge: 'Custom',
        challengeCheck: false
      }
    ];

    this.slideContents = [
      {
        header: 'START SOMETHING NEW!',
        image: 'pencil',
        description: 'Write a short term goal to form or break a habit for 7 days. We will make sure you stay focussed for the entire week and to remind you to complete your goal.'
      },
      {
        header: 'READ THE STORY!',
        image: 'onbook',
        description: 'Read the story of Colin, whose life is going be affected based on whether you completed your habit. These are byte-sized chapters told in different formats. Story starts as a chat fiction and takes other formats such as Comics and Visual Novel.'
      },
      {
        header: 'HELP COLIN!',
        image: 'newman',
        description: 'If you complete your goal today, Colin’s day will be great tomorrow. If you fail in your goal completion, Colin will have very unlucky next day. Help Colin to become "Super Hero"! '
      },
      {
        header: 'GET MOTIVATED!',
        image: 'motivate',
        description: 'The Story is designed in such a way that it will change based on your goal completion. We believe stories are powerful, and hence will make us motivated!'
      }
    ]
  }

  slideChanged = (slides) => {
    this.slides.getActiveIndex().then(index => {
      if (index === 4) {
        const a = Array.from(document.getElementsByClassName('swiper-pagination-bullets') as HTMLCollectionOf<HTMLElement>)
        a[0].style.display = 'none';
      }
      else {
        const a = Array.from(document.getElementsByClassName('swiper-pagination-bullets') as HTMLCollectionOf<HTMLElement>)
        a[0].style.display = 'block';
      }
    });
  }

  challengeSelect = (challenge, id, i) => {
    challenge.challengeCheck = !challenge.challengeCheck;
    const filteredItems = this.challenges.filter((item, index) => i !== index);

    filteredItems.map((item) => {
      if (item.challengeCheck) {
        item.challengeCheck = !challenge.challengeCheck;
      }
    });

    const selectedChanllege = {
      challenge: challenge,
      id: id,
      selected: challenge.challengeCheck
    };

    if (challenge.challengeCheck) {
      this.storage.set('selectedChanllege', selectedChanllege);
      this.isSelected = true;

      // if (selectedChanllege.id === 8) {
      //   this.router.navigate(['custom-challenge']);
      // }
      // else {
      //   this.router.navigate(['habit-modal']);
      // }
    }
    else {
      this.storage.remove('selectedChanllege');
      this.isSelected = false;
    }
  }

  goToChallenge = () => {
    this.storage.get('selectedChanllege').then((data) => {

      if (data.id === 8) {
        this.router.navigate(['custom-challenge']);
      }
      else {
        this.router.navigate(['habit-modal']);
      }
    });
  }
}

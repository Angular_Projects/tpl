import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../providers/chat-service';
import { Events } from '../../providers/events';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { HomeModalPage } from '../home-modal/home-modal.page';

@Component({
  selector: 'app-custom-challenge',
  templateUrl: './custom-challenge.page.html',
  styleUrls: ['./custom-challenge.page.scss'],
})
export class CustomChallengePage implements OnInit {
  public customForm: FormGroup;
  customSubmit: boolean;
  toUser: { toUserId: string, toUserName: string, title: string, day: number };
  userName: any;
  customObject: any[] = [];
  subscription: any;

  constructor(private storage: Storage, public events: Events, public chatService: ChatService,
    public router: Router, private screenOrientation: ScreenOrientation, private platform: Platform, public modalController: ModalController, private alertCtrl: AlertController, private formBuilder: FormBuilder) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.customForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      nickName: ['', Validators.required],
      challengeName: ['', Validators.required],
      day1: ['', Validators.required],
      day2: ['', Validators.required],
      day3: ['', Validators.required],
      day4: ['', Validators.required],
      day5: ['', Validators.required],
      day6: ['', Validators.required],
      day7: ['', Validators.required],
      day8: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.customSubmit = false;
  }
  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.customBackClick();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  customBackClick = () => {
    this.router.navigate(['intro']);
  }

  goToTabs = () => {
    this.storage.ready().then(() => {
      this.storage.set('firstName', this.customForm.value.firstName);
      this.storage.set('nickName', this.customForm.value.nickName);
      this.storage.set('challengeName', this.customForm.value.challengeName);
      this.storage.set('customForm', this.customObject);

      this.storage.get('firstName').then((val) => {
        this.userName = val;
      });

      this.toUser = {
        toUserId: '210000198410281948',
        toUserName: this.userName,
        title: 'Chapter 1 - Two Worlds',
        day: 1
      };

      this.openHomeModal();
      this.events.publish('modalOpen');
      this.modalController.dismiss();
    });
  }

  saveDay = (challengeName) => {
    switch (challengeName) {
      case 'day1':
        const day1Obj = {
          day: 1,
          title: this.customForm.value.day1
        }

        const objIndex1 = this.customObject.some((obj => obj.day === 1));

        if (objIndex1) {
          this.customObject.map((item) => {
            if (item.day === 1) {
              item['title'] = this.customForm.value.day1
            }
          });
        }
        else {
          this.customObject.push(day1Obj);
        }

        this.chatService.notifyPlayer('Day 1 challenge is saved');

        break;

      case 'day2':
        const day2Obj = {
          day: 2,
          title: this.customForm.value.day2
        }

        const objIndex2 = this.customObject.some((obj => obj.day === 2));

        if (objIndex2) {
          this.customObject.map((item) => {
            if (item.day === 2) {
              item['title'] = this.customForm.value.day2
            }
          });
        }
        else {
          this.customObject.push(day2Obj);
        }

        this.chatService.notifyPlayer('Day 2 challenge is saved');
        break;

      case 'day3':
        const day3Obj = {
          day: 3,
          title: this.customForm.value.day3
        }
        const objIndex3 = this.customObject.some((obj => obj.day === 3));

        if (objIndex3) {
          this.customObject.map((item) => {
            if (item.day === 3) {
              item['title'] = this.customForm.value.day3
            }
          });
        }
        else {
          this.customObject.push(day3Obj);
        }

        this.chatService.notifyPlayer('Day 3 challenge is saved');

        break;

      case 'day4':
        const day4Obj = {
          day: 4,
          title: this.customForm.value.day4
        }


        const objIndex4 = this.customObject.some((obj => obj.day === 4));

        if (objIndex4) {
          this.customObject.map((item) => {
            if (item.day === 4) {
              item['title'] = this.customForm.value.day4
            }
          });
        }
        else {
          this.customObject.push(day4Obj);
        }

        this.chatService.notifyPlayer('Day 4 challenge is saved');
        break;

      case 'day5':
        const day5Obj = {
          day: 5,
          title: this.customForm.value.day5
        }

        const objIndex5 = this.customObject.some((obj => obj.day === 5));
        if (objIndex5) {
          this.customObject.map((item) => {
            if (item.day === 5) {
              item['title'] = this.customForm.value.day5
            }
          });
        }
        else {
          this.customObject.push(day5Obj);
        }

        this.chatService.notifyPlayer('Day 5 challenge is saved');
        break;

      case 'day6':
        const day6Obj = {
          day: 6,
          title: this.customForm.value.day6
        }


        const objIndex6 = this.customObject.some((obj => obj.day === 6));

        if (objIndex6) {
          this.customObject.map((item) => {
            if (item.day === 6) {
              item['title'] = this.customForm.value.day6
            }
          });
        }
        else {
          this.customObject.push(day6Obj);
        }

        this.chatService.notifyPlayer('Day 6 challenge is saved');
        break;

      case 'day7':
        const day7Obj = {
          day: 7,
          title: this.customForm.value.day7
        }


        const objIndex7 = this.customObject.some((obj => obj.day === 7));
        if (objIndex7) {
          this.customObject.map((item) => {
            if (item.day === 7) {
              item['title'] = this.customForm.value.day7
            }
          });
        }
        else {
          this.customObject.push(day7Obj);
        }

        this.chatService.notifyPlayer('Day 7 challenge is saved');
        break;

      case 'day8':
        const day8Obj = {
          day: 8,
          title: this.customForm.value.day8
        }

        const objIndex8 = this.customObject.some((obj => obj.day === 8));

        if (objIndex8) {
          this.customObject.map((item) => {
            if (item.day === 8) {
              item['title'] = this.customForm.value.day8
            }
          });
        }
        else {
          this.customObject.push(day8Obj);
        }

        this.chatService.notifyPlayer('Day 8 challenge is saved');
        break;

      default:
        break;
    }
  }

  async openHomeModal() {
    const myModal = await this.modalController.create({
      component: HomeModalPage,
      componentProps: {
        homeData: this.toUser,
      },
      cssClass: 'home-modal-css',
      backdropDismiss: true
    });

    return await myModal.present();
  }

}

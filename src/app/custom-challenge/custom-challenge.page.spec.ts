import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomChallengePage } from './custom-challenge.page';

describe('CustomChallengePage', () => {
  let component: CustomChallengePage;
  let fixture: ComponentFixture<CustomChallengePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomChallengePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomChallengePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

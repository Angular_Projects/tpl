import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomChallengePage } from './custom-challenge.page';

const routes: Routes = [
    {
        path: '',
        component: CustomChallengePage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CustomChallengePageRoutingModule { }

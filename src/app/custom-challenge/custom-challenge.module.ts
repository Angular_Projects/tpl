import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { IonicModule } from '@ionic/angular';

import { CustomChallengePageRoutingModule } from './custom-challenge-routing.module';

import { CustomChallengePage } from './custom-challenge.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomChallengePageRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  declarations: [CustomChallengePage]
})
export class CustomChallengePageModule { }

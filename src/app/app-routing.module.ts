import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { OnboardGuard } from './guards/onboard.guard';

const routes: Routes = [

  { path: '', redirectTo: 'intro', pathMatch: 'full' },
  // { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  // { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule', canActivate: [OnboardGuard] },
  // { path: 'habit', loadChildren: './habit/habit.module#HabitPageModule' },
  // { path: 'chat', loadChildren: './chat/chat.module#ChatPageModule' },
  // { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  // { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  // { path: 'modal', loadChildren: './modal/modal.module#ModalPageModule' },
  // {
  //   path: '',
  //   loadChildren: './tabs/tabs.module#TabsPageModule',
  //   canActivate: [OnboardGuard] // <-- apply here 
  // },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then(m => m.IntroPageModule),
    canActivate: [OnboardGuard]
  },
  {
    path: 'habit',
    loadChildren: () => import('./habit/habit.module').then(m => m.HabitPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then(m => m.ChatPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./modal/modal.module').then(m => m.ModalPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [OnboardGuard] // <-- apply here 
  },
  {
    path: 'home-modal',
    loadChildren: () => import('./home-modal/home-modal.module').then(m => m.HomeModalPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./privacy/privacy.module').then(m => m.PrivacyPageModule)
  },
  {
    path: 'habit-modal',
    loadChildren: () => import('./habit-modal/habit-modal.module').then(m => m.HabitModalPageModule)
  },
  {
    path: 'journals',
    loadChildren: () => import('./journals/journals.module').then(m => m.JournalsPageModule)
  },
  {
    path: 'journal',
    loadChildren: () => import('./journal/journal.module').then(m => m.JournalPageModule)
  },
  {
    path: 'popovercomponent',
    loadChildren: () => import('./popovercomponent/popovercomponent.module').then(m => m.PopovercomponentPageModule)
  },
  {
    path: 'visual-novel',
    loadChildren: () => import('./visual-novel/visual-novel.module').then(m => m.VisualNovelPageModule)
  },
  {
    path: 'visual-graphics',
    loadChildren: () => import('./visual-graphics/visual-graphics.module').then(m => m.VisualGraphicsPageModule)
  },
  {
    path: 'sub-step-detail',
    loadChildren: () => import('./sub-step-detail/sub-step-detail.module').then(m => m.SubStepDetailPageModule)
  },
  {
    path: 'custom-challenge',
    loadChildren: () => import('./custom-challenge/custom-challenge.module').then(m => m.CustomChallengePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

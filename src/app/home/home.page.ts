import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ChatService } from '../../providers/chat-service';
import { HomeModalPage } from '../home-modal/home-modal.page';
import { Platform } from '@ionic/angular';
import * as introJs from 'intro.js/intro.js';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Events } from '../../providers/events';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  toUser: { toUserId: string, toUserName: string, day: number };
  clist: { countries };
  clistfull: { countries };
  storypointerDay: any;
  name: any;
  data: any;
  displayTimer = '1';
  startTimer = '1';
  totalcDates = 0;
  userName: any;
  totaldays: any;
  subscription: any;
  dataReturned: any;
  defaultBehaviours: any;
  isDefault = false;
  homeTourComplete: boolean;
  unlockchapters: any;
  colinsDay: any;

  constructor(private chatService: ChatService, private storage: Storage, public events: Events, private screenOrientation: ScreenOrientation,
    // tslint:disable-next-line: align
    public modalController: ModalController, private platform: Platform, public router: Router) {
    this.storage.get('defaultBehaviour').then((data) => {
      this.isDefault = true;
      this.defaultBehaviours = data;
    });

    events.subscribe('backPressed', () => {
      this.storage.get('defaultBehaviour').then((data) => {
        this.isDefault = true;
        this.defaultBehaviours = data;
        document.getElementById('home-id').scrollIntoView();
      });

      this.storage.get('storypointerDayDB').then((val) => {
        if (!isNaN(val)) {
          this.storypointerDay = val;
          this.colinsDay = val - 1;
        }
      });
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.storage.get('defaultBehaviour').then((data) => {
      this.isDefault = true;
      this.defaultBehaviours = data;
    });
  }

  ionViewWillEnter() {
    this.storage.get('homeTourComplete').then((val) => {
      if (!val) {
        document.getElementById('home-id').scrollIntoView();
        introJs(document.querySelector('app-home')).setOptions({
          steps: [
            {
              intro: `Begin tour for story page`

            },
            {
              element: '#home-step1',
              intro: `Current status of Colin's progress`
            },
            {
              element: '#home-step2',
              intro: `Here you can find Colin's details such as health, wanted level, and net worth`
            },
            {
              intro: 'Below you can start reading the chapters which are unlocked.'
            }
          ],
          scrollToElement: false,
          exitOnOverlayClick: false,
          showStepNumbers: false,
          showProgress: true,
          skipLabel: "Skip",
          doneLabel: "Done",
          nextLabel: "Next",
          prevLabel: "Back"
        }).start()
          .oncomplete(() => {
            this.storage.set('homeTourComplete', true);
          });
      }
    });
  }

  ionViewDidEnter() {
    document.getElementById('home-id').scrollIntoView();

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
        this.colinsDay = val - 1;
      }
    });

    this.storage.get('defaultBehaviour').then((data) => {
      this.isDefault = true;
      this.defaultBehaviours = data;
    });

    this.storage.get('userName').then((val) => {
      this.userName = val;
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.router.navigate(['tabs/tabs/habit']);
    });
  }

  ionViewWillLeave() {
    introJs().exit()
    this.subscription.unsubscribe();
  }

  passjsonData = (data) => {
    this.storage.get('first_time').then((val) => {
      if (!isNaN(val)) {
        const now = new Date().getTime();
        const difference = now - val;
        this.totaldays = Math.trunc(difference / (1000 * 60 * 60 * 24)) + 1;
        // this.totaldays = Math.trunc(difference / (1000 * 60 * 1)) + 1;
      }
    });

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;

        if ((data < this.storypointerDay) || (data === this.storypointerDay && this.storypointerDay <= this.totaldays)) {
          this.toUser = {
            toUserId: '210000198410281948',
            toUserName: this.userName,
            day: data
          };
          this.openHomeModal();
          this.events.publish('modalOpen');
        }
        else if (data > this.totaldays) {
          if (data - this.totaldays === 2) {
            this.chatService.notifyPlayer('Please wait for 2 days to complete to get this chapter unlocked.');
          }
          if (data - this.totaldays === 1) {
            this.chatService.notifyPlayer('Please wait for 1 day to complete to get this chapter unlocked.');
          }
          if ((data - this.totaldays !== 2) && (data - this.totaldays !== 1)) {
            this.chatService.notifyPlayer(`The Chapter ${data} is currently locked. Please wait till the next day.`);
          }
        }
        else {
          this.chatService.notifyPlayer(`Please complete the previous chapter to read this chapter. `);
        }
      }
    });
  }

  async openHomeModal() {
    const myModal = await this.modalController.create({
      component: HomeModalPage,
      componentProps: {
        homeData: this.toUser,
      },
      cssClass: 'home-modal-css',
      backdropDismiss: true
    });

    return await myModal.present();
  }

  showText(data) {
    this.chatService.notifyPlayer(`The Chapter ${data} is currently locked in early access version. Please wait till the next release.`);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController, IonBackButtonDelegate, IonContent, ModalController, Platform } from '@ionic/angular';
import { Events } from '../../providers/events';

@Component({
  selector: 'app-sub-step-detail',
  templateUrl: './sub-step-detail.page.html',
  styleUrls: ['./sub-step-detail.page.scss'],
})

export class SubStepDetailPage implements OnInit {
  stepDetail: any;
  nextLineSteps: any;
  nextLineQuote: any
  nextLineToolsSteps: any;
  nextLineBoolean: boolean = false;
  nextLineBooleanTools: boolean = false;
  nextLineBooleanQuote: boolean = false;
  selectedChanllege: any;
  subscription: any;

  constructor(public router: Router, private route: ActivatedRoute, private storage: Storage, public events: Events, private platform: Platform) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.stepDetail = this.router.getCurrentNavigation().extras.state.stepDetail;

        if (this.stepDetail.steps?.split('\n').length > 0) {
          this.nextLineBoolean = true;
          this.nextLineSteps = this.stepDetail.steps.split('\n');
        }

        if (this.stepDetail.tools?.split('\n').length > 0) {
          this.nextLineBooleanTools = true;
          this.nextLineToolsSteps = this.stepDetail.tools.split('\n');
        }

        if (this.stepDetail.quote?.split('\n').length > 0) {
          this.nextLineBooleanQuote = true;
          this.nextLineQuote = this.stepDetail.quote.split('\n');
        }
      }
    });
  }

  ngOnInit() {
    this.storage.get('selectedChanllege').then((data) => {
      this.selectedChanllege = data;
    });
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.habitDetailBackClick();
    });
  }

  ionViewWillLeave() {
    // unsubscribe
    this.subscription.unsubscribe();
  }

  habitDetailBackClick = () => {
    this.events.publish('backPressed');
    this.router.navigate(['tabs/tabs/habit']);

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubStepDetailPageRoutingModule } from './sub-step-detail-routing.module';

import { SubStepDetailPage } from './sub-step-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubStepDetailPageRoutingModule
  ],
  declarations: [SubStepDetailPage]
})
export class SubStepDetailPageModule {}

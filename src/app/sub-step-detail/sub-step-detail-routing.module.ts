import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubStepDetailPage } from './sub-step-detail.page';

const routes: Routes = [
  {
    path: '',
    component: SubStepDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubStepDetailPageRoutingModule {}

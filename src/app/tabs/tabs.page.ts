import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  defaultBehaviours: any;
  toggleChecked: boolean;

  constructor(private storage: Storage, private localNotifications: LocalNotifications, private screenOrientation: ScreenOrientation) {
    this.storage.get('first_time').then((val) => {
      if (!val) {
        this.localNotifications.schedule([
          {
            id: 1,
            title: 'The Paradime - How are you feeling today? ',
            text: 'Track how you feel by entering your mood in journals.',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 12)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 2,
            title: 'The Paradime - Chapter 2 is unlocked!',
            text: 'Continue reading the next chapter - `The Green eyed monster`',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 24)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 3,
            title: 'How do you plan to spend today?',
            text: 'How we spend our days is how we spend our lives  – Charlie Gilkey`',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 48)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 4,
            title: 'The Paradime - Chapter 4 is unlocked in Comics format!',
            text: 'Terrifying ordeals of Eirene',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 72)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 5,
            title: 'Is your habit a wanderer? ',
            text: 'A habit is first a wanderer, then a guest, and finally the boss. - Hungarian Proverb!',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 96)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 6,
            title: 'The Paradime - Chapter 6 is unlocked in Visual Novel format!',
            text: 'Akrasia',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 120)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 7,
            title: 'Do you have enough motivation now?',
            text: 'Motivation is what gets you started. Habit is what keeps you going. – Jim Rohn',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 132)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 8,
            title: 'The Paradime - Chapter 7 is unlocked in Visual Novel format!',
            text: 'Exorcism',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 144)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 9,
            title: 'Enjoyed reading Paradime? ',
            text: 'Please give your reviews and feedback for Paradime',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 156)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          },
          {
            id: 10,
            title: 'Try again?',
            text: 'Hope you had a great week trying one of the challenges. You can reset in about page and start again with a new challenge!',
            trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 168)) },
            led: 'FF0000',
            smallIcon: 'res://app_notification',
            icon: 'res://icon.png'
          }
        ]);

        const startDate = new Date().getTime();
        this.storage.set('first_time', startDate);
        this.defaultBehaviours = {
          happiness: 0.3,
          health: 0.6,
          romance: 0.2,
          fear: 0.3,
          wish: 0,
          wantedLevel: 0,
          balance: '$5744.00'
        };

        this.storage.set('defaultBehaviour', this.defaultBehaviours);
        //  this.storage.set('storypointerDayDB', 1);
        this.storage.set('sendthssDB', 0);
        this.storage.set('findFateDB', 'XXXXXXXX');
        this.storage.set('todayDateDB', 1);
        this.storage.remove('fill');
        this.storage.set('narrateID', 0);
      }
      else {
        console.log('app is seen before for the first time started');
      }
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    if (this.screenOrientation.type === 'landscape') {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }

    this.storage.get('darkToggleValue').then((val) => {
      this.toggleChecked = val;
      if (val) {
        document.body.setAttribute('data-theme', 'dark');
      }
      else {
        document.body.setAttribute('data-theme', 'light');
      }
    });
  }

  onClick(event) {
    let systemDark = window.matchMedia("(prefers-color-scheme: dark)");
    systemDark.addListener(this.colorTest);
    if (event.detail.checked) {
      this.storage.set('darkToggleValue', event.detail.checked);
      // this.toggleChecked = true;
      document.body.setAttribute('data-theme', 'dark');
    }
    else {
      this.storage.set('darkToggleValue', event.detail.checked);
      // this.toggleChecked = false;
      document.body.setAttribute('data-theme', 'light');
    }
  }

  colorTest(systemInitiatedDark) {
    if (systemInitiatedDark.matches) {
      document.body.setAttribute('data-theme', 'dark');
    } else {
      document.body.setAttribute('data-theme', 'light');
    }
  }

}

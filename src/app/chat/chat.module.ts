import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChatPage } from './chat.page';
import { ChatService } from '../../providers/chat-service';
import { IonCLickDirective } from '../directives/ion-click.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const routes: Routes = [
  {
    path: '',
    component: ChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FontAwesomeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChatPage, IonCLickDirective],
  providers: [
    ChatService,
    IonCLickDirective
  ],
  exports: [
    ChatPage
  ]
})
export class ChatPageModule { }


import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AlertController, IonBackButtonDelegate, IonContent, ModalController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { ChatService, ChatMessage, UserInfo } from '../../providers/chat-service';
import { ModalPage } from '../modal/modal.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Observable, Subject } from 'rxjs';
import { Events } from '../../providers/events';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss']
})

export class ChatPage implements OnInit {
  @ViewChild(IonContent, { read: IonContent, static: true }) content: IonContent;
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;
  @ViewChild('chat_input', { read: ElementRef, static: true }) messageInput: ElementRef;
  msgView: ChatMessage[] = [];
  user: UserInfo;
  name: any;
  toUser: UserInfo;
  messageId: any;
  posts: any;
  vasarc: any;
  newmsgs: any;
  editorMsg = '';
  sendthss = 0;
  toggleDigits = 0;
  testTime = 0;
  isNewDayToday = 0;
  currentTimes = '';
  populateJson = '';
  CurrentDateTime = new Date();
  IsEndofJsonNot = 0;
  FFatesDB = 'XXXXXXXX';
  SubstrFA = '';
  indice = 0;
  substrJSON: any;
  newDayWaiting = 0;
  storypointerDay = 0;
  realtimeDay = 1;
  displayTimer = '1';
  startTimer = '1';
  totalcDates = 0;
  firstAction: string;
  secondAction: string;
  thirdAction: string;
  dataReturned: any;
  newsubstrg: any;
  subscription: any;
  pathchosen: any;
  dataReturnstring: any;
  isChapterNumber = false;
  alertOk = false;
  interactionModalDialog = false;
  modalCounter = 0;
  muteIcon = true;
  audioParam = 'sound1.ogg';
  imageParam = 'chapter1_03.jpeg'
  // bg = '../../assets/img/chatBG.png';
  getJsonFile = '';

  constructor(public chatService: ChatService, public events: Events, private screenOrientation: ScreenOrientation, public router: Router, public modalController: ModalController,
    // tslint:disable-next-line:align
    private storage: Storage, private alertCtrl: AlertController, private route: ActivatedRoute, private platform: Platform) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const toUserCopy = this.router.getCurrentNavigation().extras.state.user;
        this.toUser = cloneDeep(toUserCopy);
        this.getJsonFile = 'fill' + this.toUser.day;
        this.storage.set('routerHomeparams', this.toUser);
      }
    });

    this.chatService.getUserInfo()
      .then((res) => {
        this.user = res;
      });
    this.getpointerday();
    this.findnewdayfirstlogin();
  }

  chatBackClick = () => {
    this.events.publish('backPressed');

    // this.chatService.publishChatBackPressData('backPressed');
    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;
    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val) && val === this.toUser.day) {
        const lastAudio = [{ chapterNum: this.toUser.day, lastAudioParam: this.audioParam }];
        this.storage.set('tempAudioStore', lastAudio);
      }
    });

    this.router.navigate(['tabs/tabs/home']);
  }

  ionViewWillLeave() {
    // unsubscribe
    // this.events.unsubscribe('chat:received');
    // this.chatService.getObservable().unsubscribe();
    this.subscription.unsubscribe();
    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;
  }

  getChatAudio = () => {
    return document.getElementById('chatAudio') as HTMLAudioElement;
  }

  muteChatAudio = () => {
    const chatAudio = this.getChatAudio();
    this.muteIcon = !this.muteIcon;

    if (!this.muteIcon) {
      chatAudio.muted = true;
    }
    else {
      this.chatAudio();
    }
  }

  chatAudio = () => {
    const chatAudio = this.getChatAudio();
    chatAudio.muted = false;
    chatAudio.play();
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
        this.callnewjson(this.storypointerDay);
      }
    });

    this.storage.get('sendthssDB').then((val) => {
      if (val) {
        this.sendthss = val;
      }
    });

    this.storage.get(this.getJsonFile).then((val) => {
      if (val) {
        this.msgView = val;
      } else {
        this.msgView = [];
      }
    });

    this.findnewdayfirstlogin();
  }

  ionViewDidEnter() {
    this.storage.get('darkToggleValue').then((val) => {
      if (val) {
        document.body.setAttribute('data-theme', 'dark');
      }
      else {
        document.body.setAttribute('data-theme', 'light');
      }
    });

    this.muteIcon = true;
    this.chatAudio();
    this.storage.get('routerHomeparams').then((params) => {
      this.toUser = params;
      if (params) {
        this.isChapterNumber = true;
      }
    });

    // get message list
    this.getMsg();
    this.scrollToBottom();
    this.findNewDayStatus();

    // Subscribe to received  new message events
    const chatReceivedSubscription = this.events.subscribe('chat:received', msg => {
      this.pushNewMsg(msg);
      this.getpointerday();
    });

    chatReceivedSubscription.unsubscribe();
    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
        this.callnewjson(this.storypointerDay);
      }
    });

    this.storage.get('tempAudioStore').then((lastAudio) => {
      if (lastAudio) {
        if (lastAudio[0].chapterNum === this.toUser.day) {
          this.audioParam = lastAudio[0].lastAudioParam;
        }
        else {
          this.audioParam = 'sound1.ogg'
        }
      }
      else {
        this.audioParam = 'sound1.ogg'
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.chatBackClick();
      this.router.navigate(['tabs/tabs/home']);
      // this.events.publish('backPressed');
      // this.chatService.publishChatBackPressData('backPressed');
    });
  }

  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.events.publish('backPressed');
      //  this.chatService.publishChatBackPressData('backPressed');
    };

    if (this.alertOk) {
      const chatAudio = this.getChatAudio();
      chatAudio.muted = true;
      this.events.publish('backPressed');
      //  this.chatService.publishChatBackPressData('backPressed');
      this.router.navigate(['tabs/tabs/home']);
    }
  }

  findnewdayfirstlogin = () => {
    this.getpointerday();
    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        // Get actual storypointer
        this.storypointerDay = val;
        this.storage.get('sendthssDB').then((value) => {
          if (value) {
            this.sendthss = value;
            if (this.sendthss === 0 && (this.storypointerDay) === this.toUser.day) {
              this.callnewjson(this.storypointerDay);
            }
          }
        });
      }
    });
  }

  setCharAt = (str, index, chr) => {
    if (index > str.length - 1) {
      return str;
    }

    return str.substr(0, index) + chr + str.substr(index + 1);
  }

  callnewjson = (pointerDay) => {
    this.storage.get('findFateDB').then((vald) => {
      if (vald) {
        this.FFatesDB = vald;
      }
    });

    if (this.storypointerDay === 1 || this.storypointerDay === 2) {
      this.indice = 0;
    } else if (this.storypointerDay === 3 || this.storypointerDay === 4) {
      this.indice = 2;
    } else if (this.storypointerDay === 5 || this.storypointerDay === 6) {
      this.indice = 4;
    } else if (this.storypointerDay === 7 || this.storypointerDay === 8) {
      this.indice = 6;
    }

    this.SubstrFA = this.FFatesDB.substring(1, this.indice);
    this.SubstrFA = this.SubstrFA.replace(/X/g, '0');

    switch (this.storypointerDay) {
      case 1: {
        this.substrJSON = 'd1_op';
        break;
      }

      case 2: {
        this.substrJSON = 'd2_op';
        break;
      }

      case 3: {

        this.substrJSON = 'd3_s0';
        /*
        if (this.SubstrFA === '1') {
          this.substrJSON = 'd3_s0';
        } else if (this.SubstrFA === '0') {
          this.substrJSON = 'd3_s1';
        }
        */
        break;
      }
      /*
            case 4: {
              if (this.SubstrFA === '1') {
                this.substrJSON = 'd4_s1';
              } else if (this.SubstrFA === '0') {
                this.substrJSON = 'd4_s0';
              }
              break;
            }
      
            case 5: {
              if (this.SubstrFA === '111') {
                this.substrJSON = 'd5_s11';
              } else if ((this.SubstrFA === '000') || (this.SubstrFA === '010')) {
                this.substrJSON = 'd5_s00';
              } else if ((this.SubstrFA === '101') || (this.SubstrFA === '001') || (this.SubstrFA === '011')) {
                this.substrJSON = 'd5_s01';
              } else if (this.SubstrFA === '100' || this.SubstrFA === '110') {
                this.substrJSON = 'd5_s10';
              }
              break;
            }
      
            case 6: {
              if (this.SubstrFA === '111') {
                this.substrJSON = 'd6_s11';
              } else if ((this.SubstrFA === '000') || (this.SubstrFA === '010')) {
                this.substrJSON = 'd6_s00';
              } else if ((this.SubstrFA === '101') || (this.SubstrFA === '001') || (this.SubstrFA === '011')) {
                this.substrJSON = 'd6_s01';
              } else if (this.SubstrFA === '100' || this.SubstrFA === '110') {
                this.substrJSON = 'd6_s10';
              }
              break;
            }
      
            case 8: {
              if (this.SubstrFA === '11111' || this.SubstrFA === '11110' ||
                this.SubstrFA === '11101' || this.SubstrFA === '11011' || this.SubstrFA === '10111' || this.SubstrFA === '01111') {
                this.substrJSON = 'd8_s111';
              } else if (this.SubstrFA === '11001' || this.SubstrFA === '10101' || this.SubstrFA === '10011' || this.SubstrFA === '01101') {
                this.substrJSON = 'd8_s101';
              } else if (this.SubstrFA === '11000' || this.SubstrFA === '10100' || this.SubstrFA === '01100') {
                this.substrJSON = 'd8_s100';
              } else if (this.SubstrFA === '11100' || this.SubstrFA === '01110' || this.SubstrFA === '11010') {
                this.substrJSON = 'd8_s110';
              } else if (this.SubstrFA === '10000' || this.SubstrFA === '01000' || this.SubstrFA === '00100' ||
                this.SubstrFA === '00010' || this.SubstrFA === '00001' || this.SubstrFA === '00000') {
                this.substrJSON = 'd8_s000';
              } else if (this.SubstrFA === '10110' || this.SubstrFA === '10010' || this.SubstrFA === '01010' || this.SubstrFA === '00110') {
                this.substrJSON = 'd8_s010';
              } else if ((this.SubstrFA === '10001') || (this.SubstrFA === '00101') || (this.SubstrFA === '00011')) {
                this.substrJSON = 'd8_s001';
              } else if ((this.SubstrFA === '01011') || (this.SubstrFA === '00111')) {
                this.substrJSON = 'd8_s011';
              }
              break;
            }
      
            case 7: {
              if (this.SubstrFA === '11111' || this.SubstrFA === '11110' || this.SubstrFA === '11101' || this.SubstrFA === '11011' ||
                this.SubstrFA === '10111' || this.SubstrFA === '01111') {
                this.substrJSON = 'd7_s111';
              } else if (this.SubstrFA === '11001' || this.SubstrFA === '10101' || this.SubstrFA === '10011' || this.SubstrFA === '01101') {
                this.substrJSON = 'd7_s101';
              } else if (this.SubstrFA === '11000' || this.SubstrFA === '10100' || this.SubstrFA === '01100') {
                this.substrJSON = 'd7_s100';
              } else if (this.SubstrFA === '11100' || this.SubstrFA === '01110' || this.SubstrFA === '11010') {
                this.substrJSON = 'd7_s110';
              } else if (this.SubstrFA === '10000' || this.SubstrFA === '01000' || this.SubstrFA === '00100' ||
                this.SubstrFA === '00010' || this.SubstrFA === '00001' || this.SubstrFA === '00000') {
                this.substrJSON = 'd7_s000';
              } else if (this.SubstrFA === '10110' || this.SubstrFA === '10010' || this.SubstrFA === '01010' || this.SubstrFA === '00110') {
                this.substrJSON = 'd7_s010';
              } else if ((this.SubstrFA === '10001') || (this.SubstrFA === '00101') || (this.SubstrFA === '00011')) {
                this.substrJSON = 'd7_s001';
              } else if ((this.SubstrFA === '01011') || (this.SubstrFA === '00111')) {
                this.substrJSON = 'd7_s011';
              }
              break;
            }
      
       */

      default: {
        break;
      }
    }

    this.storage.get('pathchooseDB').then((val) => {
      if (this.substrJSON) {
        if (val) {
          this.newsubstrg = this.substrJSON + '_0' + val;
          this.getJsonStory(this.newsubstrg);
        } else {
          this.pathchosen = 0;
          this.newsubstrg = this.substrJSON + '_0' + this.pathchosen;
          this.getJsonStory(this.newsubstrg);
        }
      }
    });
  }

  getpointerday = () => {
    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
      }
    });
  }

  async notifyPlayer(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'cancel'
        },
        {
          text: 'OK',
          handler: () => {
            this.alertOk = true;
            this.setUIBackButtonAction();
          }
        },
      ]
    });

    await prompt.present();
  }

  callMethodDebugs() {
    this.getpointerday();
    this.getCurrentDate();
    this.currentTimes = this.getCurrentDate();
  }

  getCurrentDate = () => {
    const currentDate = new Date();
    const dateNow = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getDate();

    return dateNow;
  }

  findNewDayStatus = () => {
    setInterval(() => {
      this.testTime = this.testTime + 3;
      if (this.testTime >= 27) {
        this.testTime = 0;
      }
    }, 3000);
  }

  getJsonStory = (numdata) => {
    this.populateJson = numdata;
    this.chatService.getPassHttp(this.populateJson)
      .subscribe(data => {
        this.posts = data;
      });
  }

  getMsg = () => {
    // Get mock message list
    return this.storage.get(this.getJsonFile).then((val) => {
      if (val) {
        this.msgView = val;
      } else {
        this.msgView = [];
      }
    });


  }

  performInteraction = () => {
    if (this.newmsgs.array[this.sendthss].type === 'messagemood') {
      this.storage.set('defaultBehaviour', this.newmsgs.array[this.sendthss].behaviours);
    }

    if (this.newmsgs.array[this.sendthss].type === 'message' || this.newmsgs.array[this.sendthss].type === 'messagemood' || this.newmsgs.array[this.sendthss].type === 'interimImage') {
      this.msgView.push((this.newmsgs.array[this.sendthss]));
      if (this.newmsgs.array[this.sendthss].interimAudioInStory === 'yes') {
        this.audioParam = this.newmsgs.array[this.sendthss].audioSrc;
      }
      if (this.newmsgs.array[this.sendthss].interimImageInStory === 'yes') {
        this.imageParam = this.newmsgs.array[this.sendthss].imageSrc;
      }
      this.sendthss = this.sendthss + 1;
      this.storage.set('sendthssDB', this.sendthss);
      this.storage.set(this.getJsonFile, this.msgView);
    } else if (this.newmsgs.array[this.sendthss].type === 'choice') {
      if (this.newmsgs.array[this.sendthss].interimAudioInStory === 'yes') {
        this.audioParam = this.newmsgs.array[this.sendthss].audioSrc;
      }
      this.firstAction = this.newmsgs.array[this.sendthss].message[0].message;
      this.secondAction = this.newmsgs.array[this.sendthss].message[1].message;
      this.thirdAction = this.newmsgs.array[this.sendthss].message[2].message;
      this.interactionModalDialog = true;
      const modalCount = this.modalCounter++;
      this.storage.set('modalCounter', modalCount);
      this.openModal();
    }
  }

  sendMsg = () => {
    this.newmsgs = this.posts;
    if (!this.interactionModalDialog) {
      if ((this.storypointerDay === this.toUser.day) && (this.sendthss !== this.newmsgs.array.length - 1)) {
        this.performInteraction();
      }
      else if (this.storypointerDay === this.toUser.day && (this.sendthss === this.newmsgs.array.length - 1)) {
        this.storage.set('sendthssDB', this.sendthss);
        this.storage.get('storypointerDayDB').then((val) => {
          if (val === this.toUser.day) {
            this.notifyPlayer(`You have reached the end of story ${this.storypointerDay}. Go to next Chapter`);
            this.storypointerDay = this.storypointerDay + 1;
            this.storage.set('storypointerDayDB', this.storypointerDay);
            this.storage.set('sendthssDB', 0);
            this.storage.set('pathchooseDB', 0);
            this.sendthss = 0;
          }
          else {
            this.notifyPlayer(`You have reached the end of story ${this.storypointerDay}. Go to next Chapter`);
          }
        });
      }
      else {
        this.notifyPlayer(`You have reached the end of story ${this.toUser.day}. Go to next Chapter`);
      }
      this.scrollToBottom();
    }
  }

  async openModal() {
    this.interactionModalDialog = true;
    const currentAudio = this.getChatAudio().getAttribute('src').substring(19);

    if (!this.muteIcon) {
      this.muteIcon = true;
    }
    const myModal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        firstAction: this.firstAction,
        secondAction: this.secondAction,
        thirdAction: this.thirdAction,
        audioParam: (currentAudio !== this.audioParam) ? this.audioParam : false
      },
      cssClass: 'modal-css',
      backdropDismiss: false
    });

    myModal.onDidDismiss().then((dataReturned) => {
      if (!this.muteIcon) {
        this.muteIcon = true;
      }
      this.interactionModalDialog = false;
      if (dataReturned !== null) {
        this.msgView.push((this.newmsgs.array[this.sendthss].message[dataReturned.data]));
        this.sendthss = this.sendthss + 1;
        this.storage.set('sendthssDB', this.sendthss);
        this.storage.set(this.getJsonFile, this.msgView);

        this.scrollToBottom();
        if (this.newmsgs.array[this.sendthss - 1].switch === 'yes') {
          this.newsubstrg = this.substrJSON + '_0' + dataReturned.data;
          this.getJsonStory(this.newsubstrg);
          this.newmsgs = this.posts;
          this.dataReturnstring = dataReturned.data + '';
          this.storage.set('pathchooseDB', this.dataReturnstring);
        }
      }
    });

    return await myModal.present();
  }

  pushNewMsg = (msg: ChatMessage) => {
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgView.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgView.push(msg);
    }

    this.scrollToBottom();
  }

  getMsgIndexById = (id: string) => {
    //  return this.msgList.findIndex(e => e.messageId === id); 
    return this.msgView.findIndex(e => e.messageId === id);
  }

  scrollToBottom = () => {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400);
  }
}

import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../providers/chat-service';
import { Storage } from '@ionic/storage';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import * as introJs from 'intro.js/intro.js';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { PopoverController } from '@ionic/angular';
import { PopovercomponentPage } from '../popovercomponent/popovercomponent.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Events } from '../../providers/events';

interface customForm {
  firstName: string;
  nickName: string;
  challengeName: string;
  day1: string;
  day2: string;
  day3: string;
  day4: string;
  day5: string;
  day6: string;
  day7: string;
  day8: string;
}

@Component({
  selector: 'app-habit',
  templateUrl: './habit.page.html',
  styleUrls: ['./habit.page.scss'],
})

export class HabitPage implements OnInit {
  userLoginDetails: any;
  firstName: any;
  nickName: any;
  habitName: string;
  challengeName: string;
  habitDate: any;
  FFatesDB: any;
  habitSummary: any;
  narratorTalk: any;
  pointmessage: any;
  getID: any;
  totaldays: any;
  subscription: any;
  statuspics: any;
  statusText: any;
  completedText: any;
  showday = ['1X', '2X', '3X', '4X', '5X', '6X', '7X'];
  displayHabitImage = false;
  i: any;
  j: any;
  currentDate: any;
  iconStatus: any;
  habitTourComplete: boolean;
  selectedChanllege: any;
  subSteps: any;
  subStepsFromjson: any;
  storypointerDay: any;
  customSteps: any;
  customStepsFromStorage: any[] = [];
  customArrayForm: customForm[] = [];
  habitLevel: string = 'Beginner';
  customBoolean: boolean = false;

  constructor(private localNotifications: LocalNotifications, public events: Events, public popoverController: PopoverController, private alertCtrl: AlertController, private screenOrientation: ScreenOrientation, public router: Router, public chatService: ChatService, public storage: Storage, private platform: Platform) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get('todayDateDB').then((val) => {
      this.totaldays = val;
    });

    this.storage.get('findFateDB').then((val) => {
      this.habitSummary = val;
      this.FFatesDB = val;
      this.habitDate = val.match(/1/g) !== null ? val.match(/1/g).length : 0;
    });

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
      }
    });

    events.subscribe('backPressed', () => {
      this.storage.get('storypointerDayDB').then((val) => {
        if (!isNaN(val)) {
          this.storypointerDay = val;
        }
      });
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  getHabitStory = (thisID) => {
    if (thisID != null && !isNaN(thisID)) {
      this.chatService.getHabitHttp()
        .subscribe(data => {
          this.pointmessage = data;
          this.narratorTalk = JSON.stringify(this.pointmessage.array[thisID].message);
        });
    }
  }

  ionViewWillEnter() {
    this.storage.get('habitTourComplete').then((val) => {
      this.habitTourComplete = val;

      if (!this.habitTourComplete) {
        document.getElementById('scroll-container').scrollIntoView();
        introJs(document.querySelector('app-habit')).setOptions({
          steps: [
            {
              intro: 'Begin tour to learn more about The Paradime!'
            },
            {
              element: '#step3',
              intro: 'Your goal progress will be displayed on the calender icons with a tick mark. Based on the days completed, level will be improved.'
            },
            {
              element: '#step4',
              intro: 'This button is to mark your goal as complete. Corresponding day will be updated with a tick mark.'
            },
            {
              intro: 'Below section contains the tasks for the selected challenge.'
            }
          ],
          scrollToElement: false,
          exitOnOverlayClick: false,
          showStepNumbers: false,
          showProgress: true,
          skipLabel: 'Skip',
          doneLabel: 'Done',
          nextLabel: 'Next',
          prevLabel: 'Back'
        }).start()
          .oncomplete(() => {
            this.storage.set('habitTourComplete', true);
          });
      }
    });

  }

  ionViewDidEnter() {
    document.getElementById('scroll-container').scrollIntoView();
    this.storage.get('todayDateDB').then((val) => {
      if (!isNaN(val)) {
        this.displayTime(val);
      }
    });

    if (!isNaN(this.habitDate) && this.habitDate) {
      if (this.habitDate < 4) {
        this.habitLevel = 'Beginner';
      }
      else if (this.habitDate < 6) {
        this.habitLevel = 'Intermediate';
      }
      else {
        this.habitLevel = 'Advanced';
      }
    }

    this.storage.get('customForm').then((val) => {
      this.customStepsFromStorage = val;
    });

    this.storage.get('firstName').then((val) => {
      this.firstName = val;
    });

    this.storage.get('nickName').then((val) => {
      this.nickName = val;
    });

    this.storage.get('challengeName').then((val) => {
      this.challengeName = val;
    });

    this.storage.get('habit').then((val) => {
      this.habitName = val;
    });

    this.storage.get('narrateID').then((val) => {
      this.getID = val;
      this.getHabitStory(this.getID);

      if (this.getID <= this.pointmessage?.array.length) {
        this.getID++;
      }

      if (!isNaN(this.getID)) {
        this.storage.set('narrateID', this.getID);
      }
    });

    this.storage.get('findFateDB').then((val) => {
      this.habitSummary = val;
      this.FFatesDB = val;
      this.habitDate = val.match(/1/g) !== null ? val.match(/1/g).length : 0;
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      // tslint:disable-next-line: no-string-literal
      navigator['app'].exitApp();
    });

    this.storage.get('selectedChanllege').then((data) => {
      this.selectedChanllege = data;
      if (data?.challenge.challenge === 'Custom') {
        this.customBoolean = true;
      }
    }).then(() => {
      this.chatService.getHabitSubSteps()
        .subscribe((data) => {
          this.subStepsFromjson = data;

          switch (this.selectedChanllege.id) {
            case 1:
              this.subSteps = this.subStepsFromjson['array'][0];
              break;
            case 2:
              this.subSteps = this.subStepsFromjson['array'][1];
              break;
            case 3:
              this.subSteps = this.subStepsFromjson['array'][2];
              break;
            case 4:
              this.subSteps = this.subStepsFromjson['array'][3];
              break;
            case 5:
              this.subSteps = this.subStepsFromjson['array'][4];
              break;
            case 6:
              this.subSteps = this.subStepsFromjson['array'][5];
              break;
            case 7:
              this.subSteps = this.subStepsFromjson['array'][6];
              break;
            case 8:
              this.subSteps = this.customStepsFromStorage;
              break;

            default:
              break;
          }
        });
    });
  }

  ionViewWillLeave() {
    introJs().exit()
    this.subscription.unsubscribe();
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopovercomponentPage,
      cssClass: 'my-custom-class',
      showBackdrop: true,
      event: ev

    });
    return await popover.present();
  }

  displayImage(picNum) {
    this.displayHabitImage = true;
    this.completedText = this.habitSummary.substr(0, [this.totaldays]).replace(/X/g, '0')
      + this.habitSummary.substr(this.totaldays + 1, this.habitSummary.length);

    for (let i = 0; i < 7; i++) {

      if (this.completedText[i] === 'X') {
        this.showday[i] = (i + 1) + 'X';
      }
      else if (this.completedText[i] === '0') {
        this.showday[i] = '0';
      }
      else if (this.completedText[i] === '1') {
        this.showday[i] = '1';
      }

      if (i === (this.totaldays - 1) && this.completedText[i] !== '1') {
        this.showday[i] = 'S';
      }
    }
  }

  displayTime(saveDates) {
    this.storage.get('first_time').then((val) => {
      if (!isNaN(val)) {
        const now = new Date().getTime();
        const difference = now - val;
        this.totaldays = Math.trunc(difference / (1000 * 60 * 60 * 24)) + 1;
        // this.totaldays = Math.trunc(difference / (1000 * 60 * 1)) + 1;
        this.storage.get('findFateDB').then((arrays) => {
          this.habitSummary = arrays;
          this.displayImage(this.habitSummary[this.totaldays]);
        });

        if (this.totaldays > saveDates && this.totaldays < 8) {
          this.storage.set('todayDateDB', this.totaldays);
        }
      }
    });
  }

  setCharAt = (str, index, chr) => {
    if (index > str.length - 1) {
      return str;
    }

    return str.substr(0, index) + chr + str.substr(index + 1);
  }

  completeHabit = () => {
    this.storage.get('todayDateDB').then((val) => {
      if (this.totaldays < 8) {
        if (this.FFatesDB[val - 1] === '1') {
          this.chatService.notifyPlayer(`Today's task is already marked as completed!`);
        }
        else {
          this.FFatesDB = this.setCharAt(this.FFatesDB, val - 1, '1');
          this.storage.set('findFateDB', this.FFatesDB);
          this.chatService.notifyPlayer(`Bravo! Good job on completing today's task! Colin is going to have a great day tomorrow`);
        }

        this.habitSummary = this.FFatesDB;
        this.habitDate = this.FFatesDB.match(/1/g) !== null ? this.FFatesDB.match(/1/g).length : 0;
        this.displayImage(this.habitSummary[this.totaldays]);
      }
      else {
        this.chatService.notifyPlayer(`You have reached end of early access version. You can continue
        using the journal feature until new chapters are released.`);
      }
    });
  }

  async disclaimerAlert(data, step, index) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'AGREE',
          handler: () => {
            this.storage.set('disclaimerCounter', 1);
            this.subStepAgreed(step, index);
          }
        },
      ]
    });

    await prompt.present();
  }

  subStepAgreed = (step, index) => {
    this.storage.get('first_time').then((val) => {
      if (!isNaN(val)) {
        const now = new Date().getTime();
        const difference = now - val;
        this.totaldays = Math.trunc(difference / (1000 * 60 * 60 * 24)) + 1;
        // this.totaldays = Math.trunc(difference / (1000 * 60 * 1)) + 1;
      }
    });

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;

        if (step.day <= this.totaldays) {
          const navigationExtras: NavigationExtras = {
            state: {
              stepDetail: step,
              stepIndex: index
            },
            queryParamsHandling: 'preserve'
          };

          this.storage.get('disclaimerCounter').then((val) => {
            if (val === 0 && (this.selectedChanllege.challenge.challenge === '7 minutes workout' || this.selectedChanllege.challenge.challenge === 'Learn a new language' || this.selectedChanllege.challenge.challenge === 'Webcomics with Inkscape')) {
              this.chatService.notifyPlayer(`You do not have permission to view this. Please view day 1 task first!`);
            }
            else {
              this.router.navigate(['sub-step-detail'], navigationExtras);
            }
          });


        }
        else {
          this.chatService.notifyPlayer(`You don't have permission to view this challenge, come back on day ${step.day}`);
        }
      }
    });
  }

  subStepClick = (step, index) => {
    this.storage.get('disclaimerCounter').then((val) => {
      if (val === 0 && this.storypointerDay === step.day) {
        if (this.selectedChanllege.challenge.challenge === '7 minutes workout') {
          this.disclaimerAlert(`Please note we have given these steps only as a suggestion as we are not experts in this field.
          We suggest you to do your own research to find the plan that works best for you.<br><br>
          The authors of this study don't recommend this program to people who are overweight, previously injured or elderly.
          Some of the exercises are not recommended for persons with hypertension or heart disease. <br><br>
          These workouts may not be suited for those who have health conditions. Please check with your doctor or a trainer he recommends to find a program that’s right for you.`, step, index);
        }
        else if (this.selectedChanllege.challenge.challenge === 'Learn a new language' || this.selectedChanllege.challenge.challenge === 'Webcomics with Inkscape') {
          this.disclaimerAlert(`Please note we have given these steps only as a suggestion as we are not experts in this field.<br><br>
          Please feel free to do your own research and then follow the plan that best suits for you.`, step, index);
        }
        else {
          this.subStepAgreed(step, index);
        }
      }
      else {
        this.subStepAgreed(step, index);
      }
    });
  }

  customChallengeclick = (subChallenge) => {
    const navigationExtras: NavigationExtras = {
      state: {
        stepDetail: subChallenge
        // stepIndex: index
      },
      queryParamsHandling: 'preserve'
    };

    this.router.navigate(['sub-step-detail'], navigationExtras);
  }

}

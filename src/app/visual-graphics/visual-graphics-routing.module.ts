import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualGraphicsPage } from './visual-graphics.page';

const routes: Routes = [
  {
    path: '',
    component: VisualGraphicsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualGraphicsPageRoutingModule {}

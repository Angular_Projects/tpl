import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ChatService } from '../../providers/chat-service';
import { ActivatedRoute, Router } from '@angular/router';
import { Events } from '../../providers/events';
import { IonBackButtonDelegate } from '@ionic/angular';

@Component({
  selector: 'app-visual-graphics',
  templateUrl: './visual-graphics.page.html',
  styleUrls: ['./visual-graphics.page.scss'],
})
export class VisualGraphicsPage implements OnInit {
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;
  TotalmageCount: any;
  graphicCount: number = 1;
  graphicOverflowCount: number = 1;
  graphicObj: any;
  audioParam = 'sound1.ogg';
  visualGraphicChapternum: any;
  alertOk = false;
  storypointerDay = 0;
  subscription: any;

  constructor(public chatService: ChatService, private storage: Storage, public router: Router, private events: Events,
    private route: ActivatedRoute, private alertCtrl: AlertController, private platform: Platform) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const toUserCopy = this.router.getCurrentNavigation().extras.state.user;
        this.visualGraphicChapternum = toUserCopy.day;
      }
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    document.getElementById('visual-image').scrollIntoView();

    this.chatAudio();
    this.getJsonVisualGraphics();
    this.storage.get('graphicCount').then((data) => {
      if (data !== null && !isNaN(data)) {
        this.graphicCount = data;
      }
    });

    this.storage.get('graphicOverflowCount').then((data) => {
      if (data !== null && !isNaN(data)) {
        this.graphicOverflowCount = data;
      }
    });

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val)) {
        this.storypointerDay = val;
      }
    });

    this.storage.get('tempAudioStore').then((lastAudio) => {
      if (lastAudio) {
        if (lastAudio[0].chapterNum === this.visualGraphicChapternum) {
          this.audioParam = lastAudio[0].lastAudioParam;
        }
        else {
          this.audioParam = 'sound1.ogg'
        }
      }
      else {
        this.audioParam = 'sound1.ogg'
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.vGraphicBackClick();
      this.router.navigate(['tabs/tabs/home']);
    });
  }

  ionViewWillLeave() {
    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;
    this.subscription.unsubscribe();
  }


  vGraphicBackClick = () => {
    this.events.publish('backPressed');
    this.events.subscribe('modalOpen', () => {
      console.log('modalopenssss');
    });

    // this.chatService.publishChatBackPressData('backPressed');
    const chatAudio = this.getChatAudio();
    chatAudio.muted = true;

    this.storage.get('storypointerDayDB').then((val) => {
      if (!isNaN(val) && val === this.visualGraphicChapternum) {
        const lastAudio = [{ chapterNum: this.visualGraphicChapternum, lastAudioParam: this.audioParam }];
        this.storage.set('tempAudioStore', lastAudio);
      }
    });

    this.router.navigate(['tabs/tabs/home']);
  }

  chatAudio = () => {
    const chatAudio = this.getChatAudio();
    chatAudio.muted = false;
    chatAudio.play();
  };

  getChatAudio = () => {
    return document.getElementById('chatAudio') as HTMLAudioElement;
  };

  getJsonVisualGraphics = () => {
    this.chatService.getVisualGraphic(this.visualGraphicChapternum)
      .subscribe((data) => {
        this.graphicObj = data;
        this.TotalmageCount = data['array'].length;
      });
  };

  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.events.publish('backPressed');
      //  this.chatService.publishChatBackPressData('backPressed');
    };

    if (this.alertOk) {
      const chatAudio = this.getChatAudio();
      chatAudio.muted = true;
      this.events.publish('backPressed');
      //  this.chatService.publishChatBackPressData('backPressed');
      this.router.navigate(['tabs/tabs/home']);
    }
  }

  async notifyPlayer(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'cancel'
        },
        {
          text: 'OK',
          handler: () => {
            this.alertOk = true;
            this.setUIBackButtonAction();
          }
        },
      ]
    });

    await prompt.present();
  }

  visualImageNextClick = () => {
    document.getElementById('visual-image').scrollIntoView();

    if (this.visualGraphicChapternum === 4) {
      if (this.graphicCount < this.TotalmageCount) {
        if (this.graphicObj.array[this.graphicCount]?.interimAudioInStory === 'yes') {
          this.audioParam = this.graphicObj.array[this.graphicCount].audioSrc;
        }

        this.graphicCount++;
        this.storage.set('graphicCount', this.graphicCount);
      }
      else if (this.storypointerDay === this.visualGraphicChapternum) {
        this.storage.get('storypointerDayDB').then((val) => {
          if (val === this.visualGraphicChapternum) {
            this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
            this.storypointerDay = val + 1;
            this.storage.set('storypointerDayDB', this.storypointerDay);
            this.storage.set('sendthssDB', 0);
            // this.storage.set('graphicCount', 1);
          }
          else {
            this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
          }
        });
      }
      else {
        this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
      }
    }
    else {
      if (this.graphicOverflowCount < this.TotalmageCount) {
        if (this.graphicObj.array[this.graphicOverflowCount]?.interimAudioInStory === 'yes') {
          this.audioParam = this.graphicObj.array[this.graphicOverflowCount].audioSrc;
        }

        this.graphicOverflowCount++;
        this.storage.set('graphicOverflowCount', this.graphicOverflowCount);
      }
      else if (this.storypointerDay === this.visualGraphicChapternum) {
        this.storage.get('storypointerDayDB').then((val) => {
          if (val === this.visualGraphicChapternum) {
            this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
            this.storypointerDay = val + 1;
            this.storage.set('storypointerDayDB', this.storypointerDay);
            this.storage.set('sendthssDB', 0);
            // this.storage.set('graphicOverflowCount', 1);
          }
          else {
            this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
          }
        });
      }
      else {
        this.notifyPlayer(`You have reached the end of story ${this.visualGraphicChapternum}. Go to next Chapter`);
      }
    }

  };

  visualImageBackClick = () => {
    if (this.visualGraphicChapternum === 4) {
      if (this.graphicCount > 1) {
        this.graphicCount--;
        if (this.graphicCount !== 0) {
          this.storage.set('graphicCount', this.graphicCount);
        }
      }
    }
    else {
      if (this.graphicOverflowCount > 1) {
        this.graphicOverflowCount--;
        if (this.graphicOverflowCount !== 0) {
          this.storage.set('graphicOverflowCount', this.graphicOverflowCount);
        }
      }
    }
  };
}

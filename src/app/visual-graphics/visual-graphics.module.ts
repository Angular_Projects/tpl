import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisualGraphicsPageRoutingModule } from './visual-graphics-routing.module';

import { VisualGraphicsPage } from './visual-graphics.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualGraphicsPageRoutingModule
  ],
  declarations: [VisualGraphicsPage]
})
export class VisualGraphicsPageModule {}

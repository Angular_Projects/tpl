import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisualGraphicsPage } from './visual-graphics.page';

describe('VisualGraphicsPage', () => {
  let component: VisualGraphicsPage;
  let fixture: ComponentFixture<VisualGraphicsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualGraphicsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisualGraphicsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

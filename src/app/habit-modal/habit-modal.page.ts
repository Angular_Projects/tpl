import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HomeModalPage } from '../home-modal/home-modal.page';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ChatService } from '../../providers/chat-service';
import { Events } from '../../providers/events';

@Component({
  selector: 'app-habit-modal',
  templateUrl: './habit-modal.page.html',
  styleUrls: ['./habit-modal.page.scss'],
})

export class HabitModalPage implements OnInit {
  defaultBehaviours: any;
  firstName: any;
  nickName: any;
  habitSubmit: boolean;
  habitForm: FormGroup;
  toUser: { toUserId: string, toUserName: string, title: string, day: number };
  userName: any;
  selectedChanllege: any;

  constructor(private storage: Storage, public events: Events, public chatService: ChatService, public router: Router, private screenOrientation: ScreenOrientation, public modalController: ModalController, private formBuilder: FormBuilder) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.habitForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      nickName: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.habitSubmit = false;
  }

  ionViewDidEnter() {
    this.storage.get('firstName').then((val) => {
      this.userName = val;
    });

    this.storage.get('selectedChanllege').then((data) => {
      this.selectedChanllege = data;
    });
  }

  goToTabs = () => {
    this.storage.ready().then(() => {
      this.storage.set('firstName', this.habitForm.value.firstName);
      this.storage.set('nickName', this.habitForm.value.nickName);
      this.storage.get('firstName').then((val) => {
        this.userName = val;
      });

      this.storage.set('introComplete', true);
      this.router.navigate(['/tabs/tabs/habit']);
    });
  }
}

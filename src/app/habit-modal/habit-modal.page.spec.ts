import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HabitModalPage } from './habit-modal.page';

describe('HabitModalPage', () => {
  let component: HabitModalPage;
  let fixture: ComponentFixture<HabitModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HabitModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

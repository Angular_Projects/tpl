import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HabitModalPageRoutingModule } from './habit-modal-routing.module';

import { HabitModalPage } from './habit-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HabitModalPageRoutingModule
  ],
  declarations: [HabitModalPage]
})
export class HabitModalPageModule { }

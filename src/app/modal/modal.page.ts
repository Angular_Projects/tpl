import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  // Data passed in by componentProps
  @Input() firstAction: string;
  @Input() secondAction: string;
  @Input() thirdAction: string;
  @Input() audioParam: any;

  modalTapHeading = true;

  constructor(public router: Router, public modalController: ModalController, private storage: Storage) { }
  ngOnInit() {
    this.storage.get('modalCounter').then((count) => {
      if (count > 0) {
        this.modalTapHeading = false;
      }
    });

    const modalAudio = this.getModalAudio();
    if (modalAudio) {
      modalAudio.muted = false;
      modalAudio.play();
    }

  }

  getModalAudio = () => {
    return document.getElementById('modalAudio') as HTMLAudioElement;
  }

  firstActionClick = () => {
    this.modalController.dismiss(0);
  }

  secondActionClick = () => {
    this.modalController.dismiss(1);
  }

  thirdActionClick = () => {
    this.modalController.dismiss(2);
  }
}

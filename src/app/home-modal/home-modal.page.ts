import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home-modal',
  templateUrl: './home-modal.page.html',
  styleUrls: ['./home-modal.page.scss'],
})
export class HomeModalPage implements OnInit {

  @Input() homeData: any;
  storyModalImage: string;

  constructor(public router: Router, public modalController: ModalController, private storage: Storage, private screenOrientation: ScreenOrientation) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    switch (this.homeData.day) {
      case 1: {
        this.storyModalImage = 'storymodalimage1.png';
        break;
      }
      case 2: {
        this.storyModalImage = 'storymodalimage2.png';
        break;
      }
      case 3: {
        this.storyModalImage = 'storymodalimage3.png';
        break;
      }
      case 4: {
        this.storyModalImage = 'storymodalimage4.png';
        break;
      }
      case 5: {
        this.storyModalImage = 'storymodalimage5.png';
        break;
      }
      case 6: {
        this.storyModalImage = 'storymodalimage6.png';
        break;
      }
      case 7: {
        this.storyModalImage = 'storymodalimage7.png';
        break;
      }
      default: {
        break;
      }
    }
  }

  goToChat = () => {
    // this.storage.set('introComplete', true);
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.homeData
      },
      queryParamsHandling: 'preserve'
    };

    if (this.homeData.day === 4 || this.homeData.day === 5) {
      this.router.navigate(['visual-graphics'], navigationExtras);
      this.modalController.dismiss();
    }
    else if (this.homeData.day === 6 || this.homeData.day === 7) {
      this.router.navigate(['visual-novel'], navigationExtras);
      this.modalController.dismiss();
    }
    else {
      this.router.navigate(['chat'], navigationExtras);
      this.modalController.dismiss();
    }
  }

}

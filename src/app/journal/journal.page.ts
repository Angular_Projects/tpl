import { Component, OnInit } from '@angular/core';
import { ChatService, FeedData } from '../../providers/chat-service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-journal',
  templateUrl: './journal.page.html',
  styleUrls: ['./journal.page.scss'],
})
export class JournalPage implements OnInit {
  journalStatement: any;
  journalStatementArray = [];
  feedData: FeedData[] = [];
  i: any;
  j: any;
  emoticon: any;
  items: any;
  challenges: any;
  journalSubmitDisable = true;
  iconStatus: any;
  journalFeedData = [];
  journalButton = false;
  firstName: any;
  totaldays: any;
  subscription: any;

  constructor(public chatService: ChatService, public storage: Storage, private platform: Platform, private screenOrientation: ScreenOrientation, public router: Router,) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get('todayDateDB').then((val) => {
      this.totaldays = val;
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.items = [
      {
        id: 1,
        name: 'Happy',
        img: 'smile'
      },
      {
        id: 2,
        name: 'Sad',
        img: 'sad'
      },
      {
        id: 3,
        name: 'Bored',
        img: 'bored'
      },
      {
        id: 4,
        name: 'Angry',
        img: 'angry'
      },
      {
        id: 5,
        name: 'Awful',
        img: 'awful'
      }
    ];
  }

  ionViewDidEnter() {
    this.storage.get('firstName').then((val) => {
      this.firstName = val;
    });

    this.storage.get('feedData').then((data) => {
      const array = new Array(data);

      if (array[0]) {
        array.forEach((items) => {
          this.feedData = [];
          for (const i of items) {
            this.feedData.push(i);
          }
        });
      }

      if (this.feedData.length >= 2) {
        this.journalButton = true;
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.router.navigate(['tabs/tabs/habit']);
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  removeColoredIcon = () => {
    this.items.forEach((item) => {
      item.img = item.img.replace('Colored', '');
    });
  }

  emoticonClick = (item, index) => {
    if (item.name === 'Happy') {
      if (item.img === 'smile') {
        this.removeColoredIcon();
        this.items[index].img = 'smileColored';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      } else {
        this.items[index].img = 'smile';
        this.journalSubmitDisable = true;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      }
    } else if (item.name === 'Sad') {
      if (item.img === 'sad') {
        this.removeColoredIcon();
        this.items[index].img = 'sadColored';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      } else {
        this.items[index].img = 'sad';
        this.journalSubmitDisable = true;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      }
    } else if (item.name === 'Bored') {
      if (item.img === 'bored') {
        this.removeColoredIcon();
        this.items[index].img = 'boredColored';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      } else {
        this.items[index].img = 'bored';
        this.journalSubmitDisable = true;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      }
    } else if (item.name === 'Angry') {
      if (item.img === 'angry') {
        this.removeColoredIcon();
        this.items[index].img = 'angryColored';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      } else {
        this.items[index].img = 'angry';
        this.journalSubmitDisable = true;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      }
    } else if (item.name === 'Awful') {
      if (item.img === 'awful') {
        this.removeColoredIcon();
        this.items[index].img = 'awfulColored';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      } else {
        this.items[index].img = 'awful';
        this.journalSubmitDisable = true;
        this.emoticon = item.img;
        this.iconStatus = item.name;
      }
    }
  }

  addJournal = () => {
    this.journalSubmitDisable = true;

    const journalObj = {
      journal: (this.journalStatement && this.journalStatement.length > 0 && this.journalStatement !== undefined) ? this.journalStatement : `Feeling ${this.iconStatus}`,
      emoticon: this.emoticon,
      userName: this.firstName,
      day: this.totaldays
    };

    const array = new Array(journalObj);

    array.forEach((item) => {
      this.journalStatementArray.push(item);
    });

    this.items.forEach((item, index) => {
      item.img = item.img.replace('Colored', '');
    });

    this.storage.set('journalAddition', this.journalStatementArray);

    setTimeout(() => {
      this.storage.get('journalAddition').then((data) => {
        this.feedData.unshift(data[data.length - 1]);
        this.storage.set('feedData', this.feedData);

        if (this.feedData.length > 2) {
          this.journalButton = true;
          this.chatService.notifyPlayer(`Journal is successfully added. Click on "View All Journals" button to see the full list.`);
        }
      });
    }, 0);

    this.journalStatement = [];
  }

  viewAllJournals = () => {
    this.router.navigate(['journals']);
  }


}

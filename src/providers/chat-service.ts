import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { Events } from '../providers/events';

export class ChatMessage {
  messageId: string;
  userId: string;
  userName: string;
  userAvatar: string;
  toUserId: string;
  time: number | string;
  message: string;
  status: string;
  Day: number;
}

export class FeedData {
  journal: any;
  emoticon: any;
  userName: any;
  date: any;
  day: number;
}

export class UserInfo {
  id: string;
  name?: string;
  avatar?: string;
  title?: string;
  day?: number;
  toUserId?: number;
}

@Injectable()
export class ChatService {
  posts: any;
  constructor(private http: HttpClient, private events: Events, public alertCtrl: AlertController) {
  }

  mockNewMsg = (msg) => {
    const mockMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: '210000198410281948',
      userName: 'Hancock',
      userAvatar: './assets/to-user.jpg',
      toUserId: '140000198202211138',
      time: Date.now(),
      message: msg.message,
      status: 'success',
      Day: 0
    };

    setTimeout(() => {
      this.events.publish('chat:received', mockMsg);
    }, Math.random() * 1800);
  }

  getMsgList(): Observable<ChatMessage[]> {
    const msgListUrl = './assets/mock/msg-list.json';

    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response.array));
  }

  getPassHttp = (data) => {
    const jsoninp = '../assets/mock/chat_' + data + '_msgs.json';

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  getHabitSubSteps = () => {
    const jsoninp = '../assets/mock/subSteps_list.json';

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  getAboutSteps = () => {
    const jsoninp = '../assets/mock/about.json';

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  getHabitHttp = () => {
    const jsoninp = '../assets/mock/chat_d1_narrated.json';

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  getVisualNovel = (visualNovelChapternum) => {
    const jsoninp = `../assets/mock/visual_novel_v${visualNovelChapternum}.json`;

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  getVisualGraphic = (visualGraphicChapternum) => {
    const jsoninp = `../assets/mock/graphic_novel_v${visualGraphicChapternum}.json`;

    return this.http.get(jsoninp).pipe(map(res => res));
  }

  sendMsg = (msg: ChatMessage) => {
    return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
      .then(() => this.mockNewMsg(msg));
  }

  getUserInfo = (): Promise<UserInfo> => {
    const userInfo: UserInfo = {
      id: '140000198202211138',
      name: 'Luff',
      avatar: './assets/user.jpg',
      title: 'Chapter 0'
    };

    return new Promise(resolve => resolve(userInfo));
  }

  async notifyPlayer(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'OK'
        },
      ]
    });

    await prompt.present();
  }
}
